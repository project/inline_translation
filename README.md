# Inline Translation

## Overview
Allows adding translation to entities in a single add/edit form instead of
separate translation page.

## Screenshots

### Settings form
![Inline translation: Content translation settings](./docs/inline-translation-settings-form.png)

### Entity add form
![Inline translation: Entity add form](./docs/inline-translation-add-form.png)

### Entity edit form
![Inline translation: Entity edit form](./docs/inline-translation-edit-form.png)
