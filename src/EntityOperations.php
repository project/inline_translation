<?php

namespace Drupal\inline_translation;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBaseInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for reacting to entity events.
 *
 * @internal
 */
class EntityOperations implements ContainerInjectionInterface {

  // @see https://www.drupal.org/project/drupal/issues/2360639.
  use DependencySerializationTrait;

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The content translation manager service.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity bundle information service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Provides a way to do various entity operations.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $content_translation_manager
   *   The content translation manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity bundle information service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ContentTranslationManagerInterface $content_translation_manager,
    LanguageManagerInterface $language_manager,
    EntityTypeBundleInfoInterface $bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->contentTranslationManager = $content_translation_manager;
    $this->languageManager = $language_manager;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('content_translation.manager'),
      $container->get('language_manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * Alters entity forms to disallow concurrent editing in multiple workspaces.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   The form ID.
   *
   * @see hook_form_alter()
   */
  public function entityFormAlter(array &$form, FormStateInterface $form_state, $form_id) {
    $form_object = $form_state->getFormObject();
    if (!($form_object instanceof EntityForm)) {
      return;
    }

    // Do not trigger on the delete operation or any other unknown operation.
    // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
    if (!in_array($form_object->getOperation(), ['add', 'edit', 'default'], TRUE)) {
      return;
    }

    /** @var \Drupal\Core\Entity\RevisionableInterface $entity */
    $entity = $form_object->getEntity();
    if (!$this->isSupported($entity)) {
      return;
    }

    // Add an entity builder to process the form when it is saved.
    $form['#entity_builders'][] = [$this, 'entityBuilder'];
    // Add entity translation form elements for unified language node form.
    $this->entityFormAddField($form, $form_state);

    // Alter save the current text of the submit button so the effects of
    // \Drupal\node\NodeTranslationHandler::entityFormAlter() can be mitigated.
    if (isset($form['actions']['submit'])) {
      $form_state->set('inline_translation.submit_button', $form['actions']['submit']['#value']);
      $form['actions']['#process'][] = [$this, 'processSubmitButton'];
    }
  }

  /**
   * Processes a submit element to replace the text.
   *
   * @param array $element
   *   The submit element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The element.
   *
   * @see \Drupal\node\NodeTranslationHandler::entityFormAlter()
   * @see inline_translation_module_implements_alter()
   */
  public function processSubmitButton(array $element, FormStateInterface $form_state) {
    if (isset($element['submit'])) {
      $element['submit']['#value'] = $form_state->get('inline_translation.submit_button');
    }
    return $element;
  }

  /**
   * Alters content translation settings form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\language\Form\ContentLanguageSettingsForm
   */
  public function settingsFormAlter(array &$form, FormStateInterface $form_state) {
    $added = FALSE;
    foreach ($form['#labels'] as $entity_type_id => $label) {
      if (!$this->contentTranslationManager->isSupported($entity_type_id)) {
        continue;
      }
      foreach ($this->bundleInfo->getBundleInfo($entity_type_id) as $bundle => $bundle_info) {
        $added = TRUE;
        $title_args = [
          '@project_url' => 'https://www.drupal.org/project/inline_translation',
        ];
        $form['settings'][$entity_type_id][$bundle]['settings']['inline_translation'] = [
          '#title' => $this->t('Enable <a href="@project_url" target="_blank">inline translation</a>', $title_args),
          '#type' => 'checkbox',
          '#default_value' => $this->loadContentLanguageSettings($entity_type_id, $bundle)->getThirdPartySetting('inline_translation', 'enabled', FALSE),
          '#states' => [
            'visible' => [
              ':input[name="settings[' . $entity_type_id . '][' . $bundle . '][translatable]"]' => [
                'checked' => TRUE,
              ],
            ],
          ],
        ];
      }
    }
    if ($added) {
      $form['#submit'][] = [$this, 'settingsFormSubmit'];
    }
  }

  /**
   * Saves inline_translation settings to the language_content_settings entity.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function settingsFormSubmit(array $form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('settings') as $entity_type_id => $bundle_settings) {
      foreach ($bundle_settings as $bundle => $settings) {
        $config = $this->loadContentLanguageSettings($entity_type_id, $bundle);
        if ($settings['settings']['inline_translation'] ?? FALSE) {
          $config->setThirdPartySetting('inline_translation', 'enabled', TRUE)->save();
        }
        elseif ($config->getThirdPartySetting('inline_translation', 'enabled', FALSE)) {
          $config->unsetThirdPartySetting('inline_translation', 'enabled')->save();
        }
      }
    }
  }

  /**
   * Entity form alter.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  protected function entityFormAddField(array &$form, FormStateInterface $form_state) {
    if (!($form_state->getFormObject() instanceof EntityForm)) {
      return;
    }
    $entity = $form_state->getFormObject()->getEntity();
    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    foreach ($fields as $field_definition) {
      $this->addLanguageElements($form, $form_state, $field_definition);
    }
  }

  /**
   * Add all enabled language fields for a single field.
   */
  protected function addLanguageElements(&$form, &$form_state, FieldDefinitionInterface $field_definition) {
    // Add language specific fields to supported fields.
    if (!$this->isSupportedField($field_definition)) {
      return;
    }

    $language_manager = $this->languageManager;
    $entity = $form_state->getFormObject()->getEntity();
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'default');
    $field_name = $field_definition->getName();

    $widget = $form_display->getRenderer($field_name);
    if (!$widget instanceof WidgetBaseInterface) {
      return;
    }
    $items = $entity->get($field_name);
    $field_form = $this->getWidgetform($widget, $items, $form, $form_state);
    $widget_form = $field_form['widget'];
    unset($field_form['widget']);
    $field_form['#parents'][] = $field_name;
    $field_form['#tree'] = TRUE;

    // This element type is supplied by the Field Group module.
    $field_form['#type'] = 'horizontal_tabs';

    // Add language specific tabs.
    foreach ($this->getSortedLanguages() as $langcode => $language) {
      $field_form[$langcode] = [
        '#type' => 'details',
        '#title' => $language->getName(),
        '#group' => $field_name,
      ];
      // Add language name as suffix to avoid confusions when the fields are
      // empty or with same value.
      $suffix_args = ['@language' => $language->getName()];
      $suffix = new TranslatableMarkup('(@language)', $suffix_args);

      if ($langcode != $language_manager->getCurrentLanguage()->getId()) {
        // Override language specific values, when available.
        if ($entity->hasTranslation($langcode)) {
          $language_items = $entity->getTranslation($langcode)->get($field_name);
        }
        else {
          $language_items = $entity->get($field_name);
        }
        $language_field_form = $this->getWidgetform($widget, $language_items, $form, $form_state);
        $widget_form = $language_field_form['widget'];
        $widget_form['#field_name'] = $field_name . '-' . $langcode;
        // Reset the parents so that the element is available to save outside of
        // the main element.
        $widget_form['#parents'] = [];
        $widget_form['#parents'][] = $widget_form['#field_name'];
        // Remove the add_more for translations so the AJAX works. This has the
        // limitation that the translated field is not updated till after
        // saving.
        // @todo Add a submit handler so that the translation field has the
        //   extra row added as well.
        if (!$widget->getPluginDefinition()['multiple_values'] && isset($widget_form['add_more'])) {
          unset($widget_form['add_more']);
        }
      }

      $this->addTranslatabilityClue($widget_form, $suffix);
      $field_form[$langcode]['widget'] = $widget_form;
    }

    $form[$field_name] = $field_form;
    $form[$field_name]['#multilingual'] = TRUE;

    // Avoid HTML5 validations as the elements hidden in tabs could
    // cause confusion.
    $form['#attributes']['novalidate'] = 'novalidate';
  }

  /**
   * Gets a widget for a field.
   *
   * Works around a bug caused by required summary elements.
   *
   * @param \Drupal\Core\Field\WidgetBaseInterface $widget
   *   The widget to get the form for.
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   An array of the field values
   * @param array $form
   *   An array representing the form that the editing element will be attached
   *   to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form element array created for this field.
   *
   * @see \Drupal\text\Plugin\Field\FieldWidget\TextareaWithSummaryWidget::formElement()
   */
  private function getWidgetform(WidgetBaseInterface $widget, FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    if (isset($form['#type'])) {
      $form_type = $form['#type'];
      unset($form['#type']);
    }
    $field_form = $widget->form($items, $form, $form_state);
    if (isset($form_type)) {
      $form['#type'] = $form_type;
    }
    return $field_form;
  }

  /**
   * Gets a sorted list of languages.
   *
   * Ensures the current language comes first.
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   *   A sorted array of languages.
   */
  private function getSortedLanguages() {
    $languages = $this->languageManager->getLanguages();
    $current_language = $this->languageManager->getCurrentLanguage()->getId();
    $sorted_languages[$current_language] = $languages[$current_language];
    return $sorted_languages + $languages;
  }

  /**
   * Entity builder callback.
   *
   * Copies the form data to the translation.
   */
  public function entityBuilder($entity_type, ContentEntityInterface $entity, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $bundle = $entity->bundle();
    $current_language = $this->languageManager->getCurrentLanguage();
    $languages = $this->getSortedLanguages();
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

    // Process the translated values.
    $values = $form_state->getValues();

    foreach ($languages as $langcode => $language) {

      if ($langcode == $current_language->getId()) {
        continue;
      }
      // Get the existing translation, or create one if it doesn't currently
      // exist.
      if ($entity->hasTranslation($langcode)) {
        $translation = $entity->getTranslation($langcode);
      }
      else {
        $translation = $entity->addTranslation($langcode);
      }

      // Copy the translated values from form_state to the entity translation.
      foreach ($fields as $field_name => $field_definition) {
        if (!$this->isSupportedField($field_definition)) {
          continue;
        }
        $field_name_language = $field_name . '-' . $langcode;
        // @todo fix the issue of technically non-empty fields.
        //   e.g body field has an empty array with format:
        //   [summary = "", value = "" format = "basic_html"].
        if (isset($values[$field_name_language])) {
          // Is the file type extends FileItem then it requires special
          // handling. This works for images too.
          $definition = \Drupal::service('plugin.manager.field.field_type')->getDefinition($field_definition->getType());
          if (is_a($definition['class'], FileItem::class, TRUE)) {
            $this->handleFileField($field_name, $values, $translation);
          }
          else {
            $translation->set($field_name, $values[$field_name_language]);
          }
          unset($values[$field_name_language]);
        }
        elseif (isset($values[$field_name])) {
          $translation->set($field_name, $values[$field_name]);
        }
      }
    }
  }

  /**
   * Handle file fields as they contain file ID in different location.
   *
   * @param string $field_name
   *   The field name.
   * @param array $values
   *   Form values.
   * @param \Drupal\Core\Entity\ContentEntityInterface $translation
   *   Translation of the entity.
   */
  protected function handleFileField(string $field_name, array &$values, ContentEntityInterface $translation) {
    $field_name_language = $field_name . '-' . $translation->language()->getId();

    if (isset($values[$field_name_language][0]['fids'][0])) {
      $file_count = count($values[$field_name_language]);
      if (!$file_count) {
        return;
      }
      for ($i = 0; $i < $file_count; $i++) {
        if (!isset($values[$field_name_language][$i]['target_id']) && isset($values[$field_name_language][$i]['fids'][0])) {
          $values[$field_name_language][$i]['target_id'] = $values[$field_name_language][$i]['fids'][0];
          unset($values[$field_name_language][$i]['fids']);
        }
      }
    }
    $translation->set($field_name, $values[$field_name_language]);
  }

  /**
   * Checks if inline translation is enabled.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Returns true if inline translation enabled, FALSE otherwise.
   */
  protected function isSupported(EntityInterface $entity) {
    if (!$entity instanceof ContentEntityInterface) {
      return FALSE;
    }
    $entity_type_id = $entity->getEntityType()->id();
    $bundle = $entity->bundle();

    // 1. check translation is enabled.
    if (!$this->contentTranslationManager->isEnabled($entity_type_id, $bundle)) {
      return FALSE;
    }

    // 2. Check if inline translation is enabled.
    if (!$this->getBundleSettings($entity_type_id, $bundle)) {
      return FALSE;
    }

    // 3. Check there are languages this can be translated to.
    if (count($this->languageManager->getLanguages()) < 2) {
      return FALSE;
    }

    // 4. Check access.
    if (!content_translation_translate_access($entity)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Checks translation supported for given field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   *
   * @return bool
   *   TRUE if supported.
   */
  protected function isSupportedField(FieldDefinitionInterface $field_definition) {
    $display_options = $field_definition->getDisplayOptions('form');
    $hidden = empty($display_options) || (isset($display_options['type']) && $display_options['type'] == 'hidden');

    // Only process entity-translatable fields.
    if ($field_definition->isTranslatable() && !$hidden) {
      // Do not translate language field.
      if ($field_definition->getName() == 'langcode') {
        return FALSE;
      }
      // Ignore few field types for now.
      $field_type = $field_definition->getType();
      switch ($field_type) {
        // Created/changed date fields do not make sense as they get same value
        // for all languages.
        case 'changed':
        case 'created':

          // Comment is not a real field.
        case 'comment':

          // Path has issues to do the translation inline.
        case 'path':
          return FALSE;
      }

      return TRUE;
    }
    return FALSE;
  }

  /**
   * Inline translation form settings.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Bundle ID.
   *
   * @return bool
   *   TRUE if set.
   */
  protected function getBundleSettings(string $entity_type_id, string $bundle) {
    return $this->loadContentLanguageSettings($entity_type_id, $bundle)->getThirdPartySetting('inline_translation', 'enabled', FALSE);
  }

  /**
   * Adds a clue about the form element translatability.
   *
   * If the given element does not have a #title attribute, the function is
   * recursively applied to child elements.
   *
   * @param array $element
   *   A form element array.
   * @param string $suffix
   *   Suffix string.
   *
   * @see \Drupal\content_translation\ContentTranslationHandler::addTranslatabilityClue()
   */
  protected function addTranslatabilityClue(array &$element, string $suffix) {
    static $fapi_title_elements;

    // Elements which can have a #title attribute according to FAPI Reference.
    if (!isset($fapi_title_elements)) {
      $fapi_title_elements = array_flip([
        'checkbox',
        'checkboxes',
        'date',
        'details',
        'fieldset',
        'file',
        'item',
        'password',
        'password_confirm',
        'radio',
        'radios',
        'select',
        'text_format',
        'textarea',
        'textfield',
        'weight',
      ]);
    }

    // Update #title attribute for all elements that are allowed to have a
    // #title attribute according to the Form API Reference. The reason for this
    // check is because some elements have a #title attribute even though it is
    // not rendered; for instance, field containers.
    if (isset($element['#type']) && isset($fapi_title_elements[$element['#type']]) && isset($element['#title'])) {
      $element['#title'] .= ' ' . $suffix;

      if ($element['#type'] === 'text_format' && isset($element['summary'])) {
        $this->addTranslatabilityClue($element['summary'], $suffix);
      }
    }
    // If the current element does not have a (valid) title, try child elements.
    elseif ($children = Element::children($element)) {
      foreach ($children as $delta) {
        $this->addTranslatabilityClue($element[$delta], $suffix);
      }
    }
    // If there are no children, fall back to the current #title attribute if it
    // exists.
    elseif (isset($element['#title'])) {
      $element['#title'] .= ' ' . $suffix;
    }
  }

  /**
   * Loads a content language config entity based on the entity type and bundle.
   *
   * @param string $entity_type_id
   *   ID of the entity type.
   * @param string $bundle
   *   Bundle name.
   *
   * @return \Drupal\language\Entity\ContentLanguageSettings
   *   The content language config entity if one exists. Otherwise, returns
   *   default values.
   */
  protected function loadContentLanguageSettings(string $entity_type_id, string $bundle) {
    $config = $this->entityTypeManager->getStorage('language_content_settings')->load($entity_type_id . '.' . $bundle);
    if ($config == NULL) {
      /** @var \Drupal\language\Entity\ContentLanguageSettings $config */
      $config = $this->entityTypeManager->getStorage('language_content_settings')->create([
        'target_entity_type_id' => $entity_type_id,
        'target_bundle' => $bundle,
      ]);
    }
    return $config;
  }

}
